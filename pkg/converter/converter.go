package converter

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

func Convert(file *os.File, outputPath string) string {
	filePath := outputPath
	if filePath == "" {
		filePath = "vcfconv-" + fmt.Sprint(time.Now().UnixMilli()) + "-" + fmt.Sprint(rand.Uint64()) + ".vcf"
	}

	res, err := os.Create(filePath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer res.Close()

	scanner := bufio.NewScanner(file)

	var n, fn string
	var counter int

	for scanner.Scan() {
		txt := scanner.Text()
		if strings.HasPrefix(txt, "TEL;TYPE=CELL:") {
			addVCard(res, n, fn, txt, counter)
			counter++
		} else if strings.HasPrefix(txt, "N:") {
			n = txt[2:]
		} else if strings.HasPrefix(txt, "FN:") {
			fn = txt[3:]
			counter = 0
		}
	}

	return filePath
}

func addVCard(file *os.File, n, fn, numLine string, counter int) {
	numLine = strings.Replace(numLine, "+", "00", 1)

	file.WriteString("BEGIN:VCARD\n")
	file.WriteString("VERSION:3.0\n")
	file.WriteString(fmt.Sprintf("N:%s\n", n))
	file.WriteString(fmt.Sprintf("FN:%s\n", formatFN(fn, counter)))
	file.WriteString(fmt.Sprintf("%s\n", numLine))
	file.WriteString("END:VCARD\n")
}

func formatFN(fn string, counter int) string {
	runes := make([]string, 10)

	i := 0
	for idx, r := range fn {
		if i+len(string(r)) < 19 {
			runes = append(runes, string(r))
		} else if counter != 0 {
			runes = append(runes, fmt.Sprint(counter))
			break
		}

		if idx+len(string(r)) == len(fn) && counter != 0 {
			runes = append(runes, fmt.Sprint(counter))
			break
		}

		i += len(string(r))
	}

	return strings.Join(runes, "")
}

package main

import (
	"embed"
	"io"
	"log"
	"net/http"
	"os"

	"gitlab.com/sudoCss/vcfconv/pkg/converter"
)

//go:embed all:static
var staticFiles embed.FS

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		if r.URL.Path != "/" {
			errorHTML, _ := staticFiles.ReadFile("static/404.html")
			w.Write(errorHTML)
			return
		}

		indexHTML, _ := staticFiles.ReadFile("static/index.html")
		w.Write(indexHTML)
	})

	http.HandleFunc("POST /upload", func(w http.ResponseWriter, r *http.Request) {
		file, header, err := r.FormFile("vcfFile")
		if err != nil {
			http.Error(w, "Error retrieving file", http.StatusBadRequest)
			return
		}
		defer file.Close()

		tempFile, err := os.CreateTemp("", "upload-*.vcf")
		if err != nil {
			http.Error(w, "Error creating temporary file", http.StatusInternalServerError)
			return
		}
		defer os.Remove(tempFile.Name())

		if _, err := io.Copy(tempFile, file); err != nil {
			http.Error(w, "Error copying file", http.StatusInternalServerError)
			return
		}

		if _, err := tempFile.Seek(0, 0); err != nil {
			http.Error(w, "Error resetting file pointer", http.StatusInternalServerError)
			return
		}

		convertedFilePath := converter.Convert(tempFile, "")

		w.Header().Set("Content-Type", "text/vcard")
		w.Header().Set("Content-Disposition", "attachment; filename="+header.Filename)

		http.ServeFile(w, r, convertedFilePath)

		os.Remove(convertedFilePath)
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

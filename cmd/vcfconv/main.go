package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/sudoCss/vcfconv/pkg/converter"
)

func main() {
	inputFile := flag.String("i", "", "Input file path(required).")
	outputFile := flag.String("o", "", "Output file path.")

	flag.Parse()

	if *inputFile == "" {
		fmt.Println("You have to specify the input file path.")
		os.Exit(1)
	}

	file, err := os.Open(*inputFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	output := converter.Convert(file, *outputFile)

	fmt.Println("Your file is saved at the following path: " + output)
}

# VCF Converter

A dead simple tool to convert .vcf files exported from modern smartphones and
modern applications to be compatible with old cellphones

It cuts the names to be no more than 20 bytes(for non ASCII characters, it do
not keep unwanted half letters at the end if it can't fit the whole letter in
the 20 byte limit), and expands vCards with multiple phone numbers to multiple
vCards with the name numbered, also it replaces the plus (+) at the beginning of
any phone number with a double zero (00)

This for example:

```vcf
BEGIN:VCARD
VERSION:3.0
N:أبوعدس;أحمد;;;
FN:أحمد أبوعدس
TEL;TYPE=CELL:+9639XXXXXXXX
TEL;TYPE=CELL:+9639YYYYYYYY
TEL;TYPE=CELL:+9639ZZZZZZZZ
END:VCARD
```

will output this:

```vcf
BEGIN:VCARD
VERSION:3.0
N:أبوعدس;أحمد;;;
FN:أحمد أبوع
TEL;TYPE=CELL:009639XXXXXXXX
END:VCARD
BEGIN:VCARD
VERSION:3.0
N:أبوعدس;أحمد;;;
FN:أحمد أبوع1
TEL;TYPE=CELL:009639YYYYYYYY
END:VCARD
BEGIN:VCARD
VERSION:3.0
N:أبوعدس;أحمد;;;
FN:أحمد أبوع2
TEL;TYPE=CELL:009639ZZZZZZZZ
END:VCARD
```

🔴 IMPORTANT: this tool doesn't cover any additional use cases or edge cases you
might have since it was developed for myself to solve my own problem
transferring contacts from my smartphone to my old cellphone every time I add
new contacts. If you need additional functionality, merge requests are welcome.

## Usage

<!-- ### CLI

You can install vcfconv as a command line tool on your computer via this
command:

```sh
go install gitlab.com/sudoCss/vcfconv/cmd/vcfconv@latest
```

and then use it as follows:

```sh
/path/to/go/bin/vcfconv -i path/to/your-modern-vcf-file.vcf -o path/you/want/res.vcf
```

or if you have the go bin directory in your path variable:

```sh
vcfconv -i path/to/your-modern-vcf-file.vcf -o path/you/want/res.vcf
```

#### Command line flags

- `-i`: Input file path.
- `-o`: Output file path.
- `-h`: Help message. -->

### Web

You can use it directly from your browser via
[this link](https://vcfconv.onrender.com).

Note: it's deployed on [render.com](https://render.com/) free plan so it may
take some time if it's not been active for a while(like 50 seconds or so for the
first response)
